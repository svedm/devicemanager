﻿using System.Collections.Generic;
using System.Linq;
using System.Management;
using DeviceManagerCommon.Models;

namespace DeviceManagerCommon
{
    public class WmiHardware
    {
        private readonly ManagementClass _mangmentClass;

        public WmiHardware()
        {
            _mangmentClass = new ManagementClass("Win32_PnPSignedDriver");
        }

        public List<IGrouping<string,DeviceModel>> GetDevices()
        {
            var managmentObjects = _mangmentClass.GetInstances().Cast<ManagementObject>();
            return managmentObjects
                 .Select(x => new DeviceModel
                 {
                     DeviceId = (string)x["DeviceID"],
                     DeviceName = (string)x["DeviceName"],
                     FriendlyName = (string)x["FriendlyName"],
                     Description = (string)x["Description"],
                     DeviceClass = (string)x["DeviceClass"],
                     DriverManufacter = (string)x["Manufacturer"],
                     DriverProviderName = (string)x["DriverProviderName"],
                     DriverVersion = (string)x["DriverVersion"]
                 }).GroupBy(x => x.DeviceClass).ToList();
        }
    }
}
