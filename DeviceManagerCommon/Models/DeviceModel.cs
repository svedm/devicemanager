﻿namespace DeviceManagerCommon.Models
{
    public class DeviceModel
    {
        private string _deviceName;
        private string _deviceClass;
        private string _driverProviderName;
        private string _driverManufacter;

        public string DeviceId { get; set; }

        public string DeviceName
        {
            get { return FriendlyName ?? _deviceName ?? "Unknown device"; }
            set { _deviceName = value; }
        }

        public string Description { get; set; }

        public string DeviceClass
        {
            get { return _deviceClass ?? "ANOTHER"; }
            set { _deviceClass = value; }
        }

        public string DriverProviderName
        {
            get { return _driverProviderName ?? "Unknown"; }
            set { _driverProviderName = value; }
        }

        public string DriverVersion { get; set; }

        public string DriverManufacter
        {
            get { return _driverManufacter ?? "Unknown"; }
            set { _driverManufacter = value; }
        }

        public string FriendlyName { get; set; }
    }
}
