﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DeviceManager.Properties;
using DeviceManagerCommon;
using DeviceManagerCommon.Models;


namespace DeviceManager
{
    public partial class Form1 : Form
    {
        private WmiHardware _wmiHardware;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Visible = true;
            backgroundWorker1.RunWorkerAsync();
        }

        void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Tag != null)
            {

                var tag = (DeviceModel)e.Node.Tag;
                ClearRtb();
                AddToRtb(String.Format("Name: {0}\n", tag.DeviceName));
                AddToRtb(String.Format("Description: {0}\n", tag.Description));
                AddToRtb(String.Format("Device Id: {0}\n", tag.DeviceId));
                AddToRtb(String.Format("Driver Manufacter: {0}\n", tag.DriverManufacter));
                AddToRtb(String.Format("Driver provider name: {0}\n", tag.DriverProviderName));
                AddToRtb(String.Format("Driver version: {0}\n", tag.DriverVersion));                  
            }
                
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            _wmiHardware = new WmiHardware();
            var devices = _wmiHardware.GetDevices();
            UpdateTreeView(devices);
        }

        private void AddToRtb(string text)
        {
            if (richTextBox1.InvokeRequired)
                richTextBox1.Invoke(new Action<string>(AddToRtb),text);
            else
                richTextBox1.Text += text;
        }
        private void ClearRtb()
        {
            if (richTextBox1.InvokeRequired)
                richTextBox1.Invoke(new Action(ClearRtb));
            else
                richTextBox1.Clear();
        }

        private void UpdateTreeView(List<IGrouping<string, DeviceModel>> devices)
        {
            if (treeView1.InvokeRequired)
                treeView1.Invoke(new Action<List<IGrouping<string, DeviceModel>>>(UpdateTreeView),devices);
            else
            {
                treeView1.BeginUpdate();
                treeView1.Nodes.Clear();
                foreach (var group in devices)
                {
                    treeView1.Nodes.Add(new TreeNode(group.Key) {Name = group.Key});
                    foreach (var subgroup in group)
                    {
                        treeView1.Nodes[group.Key].Nodes.Add(new TreeNode(subgroup.DeviceName) {Tag = subgroup});
                    }
                }
                treeView1.EndUpdate();
                treeView1.AfterSelect += treeView1_AfterSelect;
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();
            backgroundWorker1.RunWorkerCompleted += (o, args) => label1.Visible = false;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MessageBox.Show(Resources.About, "About Device Manager");
        }



    }
}
